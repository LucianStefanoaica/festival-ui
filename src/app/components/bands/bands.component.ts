import { Component, OnInit } from '@angular/core';
import { BandsService } from 'src/app/services/bands.service';
import { Band } from 'src/app/components/register/register.component';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-bands',
  templateUrl: './bands.component.html',
  styleUrls: ['./bands.component.css']
})
export class BandsComponent implements OnInit {

  bands: Band[] = [];

  imageUrls = new Map();

  constructor(private bandsService: BandsService, private sanitizer: DomSanitizer) { }

  ngOnInit(): void {
    this.bandsService.getBands().subscribe(data => {
      this.bands = data as Band[];
      this.initPics();

    }, error => {
      console.log(error);
    });
  }

  private initPics() {
    this.bands.forEach(band => {
      this.bandsService.getBandPic(band.id).subscribe(data => {
        let unsafeImageUrl = URL.createObjectURL(data);
        let imageUrl = this.sanitizer.bypassSecurityTrustUrl(unsafeImageUrl);
        this.imageUrls.set(band.id, imageUrl);

      }, error => {
        console.log(error);
      });
    });
  }
}
