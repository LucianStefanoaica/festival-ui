import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { RegistrationService } from 'src/app/services/registration.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  fileToUpload: File | null = null;

  performerTypes: string[] = [
    'Single Performer',
    'Band'
  ];

  registrationForm: FormGroup;

  constructor(private registrationService: RegistrationService) {

    this.registrationForm = new FormGroup({
      name: new FormControl('', Validators.required),
      email: new FormControl('', Validators.required),
      phone: new FormControl('', Validators.required),
      ticketPrice: new FormControl('', Validators.required),
      performerType: new FormControl(this.performerTypes[0], Validators.required)
    });
  }

  ngOnInit(): void {
  }

  submitRegistration() {

    this.registrationService.registerNewBand(this.registrationForm.value).subscribe(band => {
      console.log("ID of the band you've just created: " + band.id);

      this.uploadFile(band.id);

    }, error => {
      console.log(error);
    });

  }

  handleFileInput(event: any) {
    this.fileToUpload = event.target.files.item(0);
  }

  uploadFile(bandId: number) {
    if (this.fileToUpload != null) {
      this.registrationService.postFile(bandId, this.fileToUpload).subscribe(data => {
        console.log("Upload successful");

      }, error => {
        console.log(error);
      });
    }
  }
}

export class Band {

  constructor(public id: number,
    public name: string,
    public email: String,
    public phone: String,
    public purchasePrice: number) {
  }
}
