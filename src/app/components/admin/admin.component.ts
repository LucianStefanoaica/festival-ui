import { Component, OnInit } from '@angular/core';

import { BikeService } from '../../services/bike.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  public bikes: Bike[] = [];

  constructor(private bikeService: BikeService) { }

  ngOnInit(): void {
    this.getBikes();
  }

  getBikes() {
    this.bikeService.getBikes().subscribe(
      data => { this.bikes = data as Bike[] },
      err => { console.error(err) },
      () => console.log('bikes loaded')
    );

  }
}

export class Bike {

  constructor(public id: number, 
              public name: string, 
              public email: String,
              public phone: String, 
              public model: String, 
              public purchasePrice: number, 
              public purchaseDate: Date, 
              public serialNumber: String) {
  }
}