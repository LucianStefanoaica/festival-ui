import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BikeService } from 'src/app/services/bike.service';
import { Bike } from '../admin/admin.component';


@Component({
  selector: 'app-view-registration',
  templateUrl: './view-registration.component.html',
  styleUrls: ['./view-registration.component.css']
})
export class ViewRegistrationComponent implements OnInit {

  public bikeRegistration: Bike = {
    id: 0,
    name: "",
    email: "",
    model: "",
    purchasePrice: 0,
    purchaseDate: new Date(),
    serialNumber: "",
    phone: ""
  };

  constructor(private bikeService: BikeService, private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.getBikeRegistration(this.route.snapshot.params.id);
  }

  getBikeRegistration(id: number) {
    this.bikeService.getBike(id).subscribe(data => {
      this.bikeRegistration = data as Bike;

    }, error => {
      console.error(error);

    }, () => {
      console.log("bikes loaded!");
    });
  }

}
