import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  activeComponentName: string = "";

  constructor() {
    let url = window.location.href;
    let splittedUrl = url.split("/");
    let initialComponentName = splittedUrl[splittedUrl.length - 1];

    if (initialComponentName === "") {
      initialComponentName = "home";
    }

    this.activeComponentName = initialComponentName;
  }

  ngOnInit() {
  }

  isActive(componentName: string) {
    if (componentName === this.activeComponentName) {
      return "active";

    } else {
      return "";
    }
  }

  activate(componentName: string) {
    this.activeComponentName = componentName;
  }

}