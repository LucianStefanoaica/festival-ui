import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Band } from '../components/register/register.component';

@Injectable({
  providedIn: 'root'
})
export class RegistrationService {

  constructor(private http: HttpClient) { }

  postFile(bandId: number, fileToUpload: File) {
    let url = 'server/api/bands/pics/';

    let formData = new FormData();
    formData.append('bandId', bandId.toString());
    formData.append('file', fileToUpload, fileToUpload.name);

    return this.http.post(url, formData);
  }

  registerNewBand(band: any): Observable<Band> {
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    }

    let url = 'server/api/bands/';
    let body = JSON.stringify(band);
    return this.http.post(url, body, httpOptions) as Observable<Band>;
  }


}
