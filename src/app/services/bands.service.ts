import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BandsService {

  constructor(private http: HttpClient) { }

  getBands() {
    return this.http.get('server/api/bands');
  }

  getBandPic(bandId: number) {
    return this.http.get('server/api/bands/pics/' + bandId, { responseType: 'blob' });
  }
}
