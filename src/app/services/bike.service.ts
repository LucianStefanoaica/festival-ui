import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Bike } from '../components/admin/admin.component';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
}

@Injectable({
  providedIn: 'root'
})
export class BikeService {

  constructor(private http: HttpClient) { }

  getBikes() {
    return this.http.get('server/api/v1/bikes');
  }

  getBike(id: number) {
    let url = 'server/api/v1/bikes/' + id;
    return this.http.get(url);
  }

  createBikeRegistration(bike: Bike) {
    let url = 'server/api/v1/bikes/';
    let body = JSON.stringify(bike);
    return this.http.post(url, body, httpOptions);
  }

}
