import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from './components/admin/admin.component';
import { BandsComponent } from './components/bands/bands.component';

import { HomeComponent } from './components/home/home.component';
import { LikedComponent } from './components/liked/liked.component';
import { LocationsComponent } from './components/locations/locations.component';
import { PerformersComponent } from './components/performers/performers.component';
import { RegisterComponent } from './components/register/register.component';
import { SearchComponent } from './components/search/search.component';
import { TicketsComponent } from './components/tickets/tickets.component';

import { ViewRegistrationComponent } from './components/view-registration/view-registration.component';

const routes: Routes = [{
  path: '',
  component: HomeComponent

}, {
  path: 'admin/view/:id',
  component: ViewRegistrationComponent

}, {
  path: 'admin',
  component: AdminComponent

}, {
  path: 'register',
  component: RegisterComponent

}, {
  path: 'bands',
  component: BandsComponent

}, {
  path: 'performers',
  component: PerformersComponent

}, {
  path: 'locations',
  component: LocationsComponent

}, {
  path: 'tickets',
  component: TicketsComponent
}, {
  path: 'search',
  component: SearchComponent

}, {
  path: 'liked',
  component: LikedComponent
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
