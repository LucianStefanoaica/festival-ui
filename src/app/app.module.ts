import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { BikeService } from './services/bike.service';
import { AdminComponent } from './components/admin/admin.component';
import { HomeComponent } from './components/home/home.component'
import { ReactiveFormsModule } from '@angular/forms';
import { ViewRegistrationComponent } from './components/view-registration/view-registration.component';
import { NavComponent } from './components/nav/nav.component';
import { RegisterComponent } from './components/register/register.component';
import { BandsComponent } from './components/bands/bands.component';
import { PerformersComponent } from './components/performers/performers.component';
import { LocationsComponent } from './components/locations/locations.component';
import { TicketsComponent } from './components/tickets/tickets.component';
import { SearchComponent } from './components/search/search.component';
import { LikedComponent } from './components/liked/liked.component';

@NgModule({
  declarations: [
    AppComponent,
    AdminComponent,
    HomeComponent,
    ViewRegistrationComponent,
    NavComponent,
    RegisterComponent,
    BandsComponent,
    PerformersComponent,
    LocationsComponent,
    TicketsComponent,
    SearchComponent,
    LikedComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [BikeService],
  bootstrap: [AppComponent]
})
export class AppModule { }
